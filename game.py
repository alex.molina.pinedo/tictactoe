class Game:
    # -------------------- DECLARACION DE VARIABLES --------------------
    board = {'7': ' ', '8': ' ', '9': ' ',
             '4': ' ', '5': ' ', '6': ' ',
             '1': ' ', '2': ' ', '3': ' '}

    board_keys = []

    # -------------------- DECLARACION DE VARIABLES --------------------

    def printTable(self, board):
        print(board['7'] + ' | ' + board['8'] + ' | ' + board['9'])
        print('--+---+--')
        print(board['4'] + ' | ' + board['5'] + ' | ' + board['6'])
        print('--+---+--')
        print(board['1'] + ' | ' + board['2'] + ' | ' + board['3'])

    def printWinText(self, turn):
        print("\nFIN.\n")
        print(" **** " + turn + " HA GANADO!. ****")

    def restartGame(self):
        restart = input("Quieres volver a jugar?(s/n)").lower()
        if restart == "s":
            for key in self.board_keys:
                self.board[key] = " "

            print("\n\n\n")
            self.initGame()

    def printTurn(self, turn):
        if turn == 'X':
            print("Es el turno del JUGADOR 1 ('" + turn + "')")
        else:
            print("Es el turno del JUGADOR 2 ('" + turn + "')")

    def funcsOnWinning(self, turn):
        self.printTable(self.board)
        self.printWinText(turn)

    def checkWinCondition(self, turn):
        if self.board['7'] == self.board['8'] == self.board['9'] != ' ':  # fila de arriba
            self.funcsOnWinning(turn)
            return True
        elif self.board['4'] == self.board['5'] == self.board['6'] != ' ':  # fila del medio
            self.funcsOnWinning(turn)
            return True
        elif self.board['1'] == self.board['2'] == self.board['3'] != ' ':  # fila de abajo
            self.funcsOnWinning(turn)
            return True
        elif self.board['1'] == self.board['4'] == self.board['7'] != ' ':  # columna izquierda
            self.funcsOnWinning(turn)
            return True
        elif self.board['2'] == self.board['5'] == self.board['8'] != ' ':  # columna del medio
            self.funcsOnWinning(turn)
            return True
        elif self.board['3'] == self.board['6'] == self.board['9'] != ' ':  # columna derecha
            self.funcsOnWinning(turn)
            return True
        elif self.board['7'] == self.board['5'] == self.board['3'] != ' ':  # diagonal 1
            self.funcsOnWinning(turn)
            return True
        elif self.board['1'] == self.board['5'] == self.board['9'] != ' ':  # diagonal 2
            self.funcsOnWinning(turn)
            return True
        return False

    # Añadir teclas de juego
    for key in board:
        board_keys.append(key)

    # ------------------------------ JUEGO ------------------------------
    def initGame(self, l=None):
        turn = 'X'
        turnCounter = 0

        # Maximo son 9 tiradas en total para todo el juego
        for i in range(10):
            self.printTable(self.board)
            self.printTurn(turn)

            # do while para registrar el siguiente movimiento
            while True:
                move = input()
                if self.board[move] == ' ':
                    self.board[move] = turn
                    turnCounter += 1
                    break
                else:
                    print("Esta casilla ya esta en uso.\nEn que posicion quieres mover?")
                    self.printTable(self.board)

            # Despues del turno 5 ya se puede ganar asi que comprobaremos las posibilidades
            if turnCounter >= 5:
                if self.checkWinCondition(turn):
                    if turn == 'X':
                        tmp_text = "GANADOR JUGADOR 1"
                        l.AddVariable(tmp_text)
                    else:
                        tmp_text = "GANADOR JUGADOR 2"
                        l.AddVariable(tmp_text)
                    l.UpdateLog()
                    break

            # Si el contador llega a 9 sin ganadores es un empate.
            if turnCounter == 9:
                print("EMPATE!!")
                self.printTable(self.board)
                self.restartGame()

            # Seleccionar el turno correspondiente
            turn = 'O' if turn == 'X' else 'X'
            l.UpdateLog()

        self.restartGame()