import time

class Logs:

    f = None
    variables = []

    def OpenFile(self, mode):
        timestr = time.strftime("%Y%m%d-%H%M%S")
        finalstr = "Log " + timestr + ".txt"
        if mode == "r":
            self.f = open(finalstr, "r")
        elif mode == "a":
            self.f = open(finalstr, "a")
        elif mode == "w":
            self.f = open(finalstr, "w")
        elif mode == "x":
            self.f = open(finalstr, "x")
        else:
            return False
        return True

    def CloseFile(self):
        self.f.close()
        return True

    def AddVariable(self, v):
        self.variables.append(v)
        return True

    def UpdateLog(self):
        for i in range(len(self.variables)):
            if type(self.variables[i]) == list:
                for x in range(len(self.variables[i])):
                    self.f.write(self.variables[i][x])
                    self.f.write('\n')
            elif type(self.variables[i]) == dict:
                self.f.write(self.variables[i]['7'] + ' | ' + self.variables[i]['8'] + ' | ' + self.variables[i]['9'] + '\n')
                self.f.write('--+---+--' + '\n')
                self.f.write(self.variables[i]['4'] + ' | ' + self.variables[i]['5'] + ' | ' + self.variables[i]['6'] + '\n')
                self.f.write('--+---+--' + '\n')
                self.f.write(self.variables[i]['1'] + ' | ' + self.variables[i]['2'] + ' | ' + self.variables[i]['3'] + '\n')
                self.f.write('\n\n\n')
            else:
                self.f.write(self.variables[i])
                self.f.write('\n')