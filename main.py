from logs import Logs
from game import Game

# -------------------- DECLARACION DE VARIABLES --------------------
g = Game()
l = Logs()

# ------------------------------ MAIN ------------------------------
if __name__ == '__main__':
    l.OpenFile("a")
    l.AddVariable(g.board)
    g.initGame(l)
    l.CloseFile()